#ifndef XETN_UTIL_ARRAYLIST
#define XETN_UTIL_ARRAYLIST

typedef struct array_list {
	void*  *list;
	size_t cap;
	size_t len;
} ArrayList_t, *ArrayList;

ArrayList ArrayList_init(ArrayList, size_t);
void      ArrayList_free(ArrayList, void (*)(void*));
void      ArrayList_push(ArrayList, void*);
void*     ArrayList_pop(ArrayList);
void*     ArrayList_top(ArrayList);
void      ArrayList_set(ArrayList, size_t, void*);
void*     ArrayList_get(ArrayList, size_t);
#define   ArrayList_length(l)   ((l)->len)
#define   ArrayList_capacity(l) ((l)->cap)
#define   ArrayList_clear(l)    ((l)->len = 0)

#endif /* XETN_UTIL_ARRAYLIST */
