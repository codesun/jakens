#include <stdlib.h>
#include "arraylist.h"

ArrayList ArrayList_init(ArrayList self, size_t len) {
	self->len = 0;
	self->cap = len;
	self->list = (void*)malloc(sizeof(void*) * len);
	return self;
}

void ArrayList_free(ArrayList self, void (*free_cb)(void*)) {
	void** list = self->list;
	if(free_cb) {
		for(size_t i = 0; i < self->len; ++i) {
			free_cb(list[i]);
		}
	}
	self->len = 0;
	self->cap = 0;
	free(list);
}

void ArrayList_push(ArrayList self, void* content) {
	size_t cur = self->len;
	size_t cap = self->cap;
	if(cur >= cap) {
		while(cur >= cap) {
			cap <<= 1;
		}
		self->list = (void**)realloc(self->list, sizeof(void*) * cap);
		self->cap  = cap;
	}
	self->list[cur] = content;
	self->len = cur + 1;
}

void* ArrayList_pop(ArrayList self) {
	if(self->len > 0) {
		return self->list[--self->len];
	}
	return NULL;
}

void* ArrayList_top(ArrayList self) {
	size_t len = self->len;
	if(len > 0) {
		return self->list[len - 1];
	}
	return NULL;
}

void ArrayList_set(ArrayList self, size_t index, void* content) {
	if(index >= 0 && index < self->len) {
		self->list[index] = content;
	}
}

void* ArrayList_get(ArrayList self, size_t index) {
	if(index >= 0 && index < self->len) {
		return self->list[index];
	}
	return NULL;
}

