CC = gcc

all: libjakens.a libjakens.so

libjakens.a: jakens.c jpath.c arraylist.c 
	$(CC) -O2 -c $^
	ar -r $@ *.o

libjakens.so: jakens.c jpath.c arraylist.c
	$(CC) -fPIC -shared -O2 $^ -o $@

.PHONY: clean

clean:
	rm *.o *.a *.so

