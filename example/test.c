#include "../jakens.h"
#include <stdio.h>

int main() {
	JsonDocument_t doc;
	JsonParser_t parser;
	JsonParser_init(&parser);
	JsonDocument res = Json_parseFromFile(&parser, "test.json", &doc);
	if(res == NULL) {
		printf("ERROR!\n Reason: %s\n", JsonParser_getErrorMsg(&parser));
		return -1;
	}
	JsonParser_close(&parser);

	JPath_t path;
	JPath_init(&path);
	printf("========================\n");
	char buf[1024];
	while(1) {
		printf("Enter the path:\n");
		scanf("%s", buf);
		JPath pres = JPath_compile(&path, buf);
		if(pres == NULL) {
			printf("Invalid path!\n");
			continue;
		}
		JsonElement rr = JsonDocument_findElement(&doc, pres);
		if(rr == NULL) {
			printf("No such element!\n");
			continue;
		}
		printf("RES: ");
		switch(rr->type) {
			case JSON_NULL:
				printf("null\n");
				break;
			case JSON_BOOLEAN:
				printf(rr->val.bol == 1 ? "true\n" : "false\n");
				break;
			case JSON_NUMBER:
				printf("%lf\n", rr->val.num);
				break;
			case JSON_STRING:
				printf("%s\n", rr->val.str);
				break;
			case JSON_ARRAY:
				printf("ARRAY\n");
				break;
			case JSON_OBJECT:
				printf("OBJECT\n");
				break;
		}
	}
	JPath_free(&path);
	JsonDocument_free(&doc);
	return 0;
}

