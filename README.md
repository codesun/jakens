# jakens
A json parser for C.

## Feature

1. Support partial parsing.
2. Support element locating with JPath.

## Element Type

`JsonElement` is the generic type for different json type, the real types are listed below:

+ JSON_STRING
+ JSON_NUMBER
+ JSON_OBJECT
+ JSON_ARRAY
+ JSON_BOOLEAN
+ JSON_NULL

You can obtain the type with `JsonElement_getType` macro.

## Element Value

As for getting the value of `JsonElement`, you can use the following macros:

+ `JsonElement_isNull`
+ `JsonElement_getStr`
+ `JsonElement_getBool`
+ `JsonElement_getNum`

So how do we get the content of `JsonArray` and `JsonObject`? There are sereval functions to finish the task.

+ `JsonObject_getElement(JsonElement, const char*)`
+ `JsonArray_getElement(JsonElement, uint32_t)`

If you want to iterate every element inside these 2 container, there are 2 macro:

+ `JsonObject_foreach(E, P)`
+ `JsonArray_foreach(E, P)`

You can use them just like `for` expression, where P is the iterator.

## Json Parser

`JsonParser` has 3 functions, which can initialize/close the parser and get the error message respectively.

+ `JsonParser_init(JsonParser)`
+ `JsonParser_close(JsonParser)`
+ `JsonParser_getErrorMsg(JsonParser)`

## Json Document

`JsonDocument` is the DOM tree of the specific json file.

There are 2 functions to get the `JsonDocument`:

+ `Json_parseFromString(JsonParser, const char*, size_t, JsonDocument)`
+ `Json_parseFromFile(JsonParser, const char*, JsonDocument)`

You can destory `JsonDocument` with:

+ `JsonDocument_free(JsonDocument)`

`JsonDocument` has an unique root, with which you can start getting the content. You can get the root with:

+ `JsonDocument_getRoot(JsonDocument)`

## Json Path

`JPath` is a kind of access path that jakens provides. With which we can locate the target content easily.

For example, we have the following json template:

```
{
	"nperson": 2.
	"persons": [
		{
			"name": "a",
			"age": 21
		},
		{
			"name": "b",
			"age": 22
		}
	]
}
```

What if we want to get the age of person `a`?

We can use the `JPath`: `$.persons[0].age`.

`$` means the root element, `[]` is used to get the element from `JsonArray`, `.` is used to get the element from `JsonObject`.

**Limitation**: the key of `JsonElement`, can only consist of alphabet, number and underline (`[_a-zA-Z0-9]`).
